//
//  WeatherEndPoint.swift
//  BeerList
//
//  Created by Alvaro IDJOUBAR on 14/02/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation

public enum AdsEndPoint {
    case ads
    case categories
}

extension AdsEndPoint: EndPointType {

    var baseURL: URL {
        switch self {
        case .ads, .categories:
            guard let url = URL(string: "https://raw.githubusercontent.com") else { fatalError("baseURL could not be configured.")}
            return url
        }
    }
    
    var path: String {
        switch self {
        case .ads:
            return "/leboncoin/paperclip/master/listing.json"
        case .categories:
            return "leboncoin/paperclip/master/categories.json"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .ads, .categories:
            return .get
        }
    }
    
    var task: HTTPTask {
        switch self {
        case .ads, .categories:
            return .request
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .ads, .categories:
            return nil
        }
    }
}

