//
//  WeatherNetworkManager.swift
//  BeerList
//
//  Created by Alvaro IDJOUBAR on 14/02/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation

struct AdsNetworkManager: NetworkManager {

    let router = Router<AdsEndPoint>()

    func getModel<T: Decodable>(ofType: T.Type, endPoint: AdsEndPoint, completion: @escaping (Result<T, NetworkErrorResponse>) -> ()) {
        
        router.request(endPoint) { result in
            switch result {
            case .failure(_):
                completion(.failure(NetworkErrorResponse.failed))
            case .success(let data):
                do {
                    let apiResponse = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(apiResponse))
                } catch (let error) {
                    print(error)
                    completion(.failure(NetworkErrorResponse.unableToDecode))
                }
            }
        }
    }
}

// HANDLE ERRORS WITH CODES - 404, 500, etc .....
