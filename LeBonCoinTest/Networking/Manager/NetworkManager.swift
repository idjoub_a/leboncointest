//
//  NetworkManager.swift
//  BeerList
//
//  Created by Alvaro IDJOUBAR on 14/02/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation

protocol NetworkManager {
    associatedtype MyRouter
    var router: MyRouter { get }
    func getModel<T: Decodable>(ofType: T.Type, endPoint: AdsEndPoint, completion: @escaping (Result<T, NetworkErrorResponse>) -> ())
}

enum NetworkErrorResponse: Error {
    case authenticationError
    case badRequest
    case outdated
    case failed
    case noData
    case unableToDecode
    
    var description: String {
        switch self {
        case .authenticationError:
            return "You need to be authenticated first."
        case .badRequest:
            return "Bad request"
        case .outdated:
            return "The url you requested is outdated."
        case .failed:
            return "Network request failed."
        case .noData:
            return "Response returned with no data fetch from database."
        case .unableToDecode:
            return "We could not decode the response."
        }
    }
}
