//
//  UIViewExtension.swift
//  LeBonCoinTest
//
//  Created by Alvaro IDJOUBAR on 24/07/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

extension UIView {

    func sizedAnchors(width: CGFloat, height: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false

        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
    func anchors(left: NSLayoutXAxisAnchor? = nil,
                 top: NSLayoutYAxisAnchor?  = nil,
                 right: NSLayoutXAxisAnchor? = nil,
                 bottom: NSLayoutYAxisAnchor? = nil,
                 centerX: NSLayoutXAxisAnchor? = nil,
                 centerY: NSLayoutYAxisAnchor? = nil,
                 paddingLeft: CGFloat = 0,
                 paddingTop: CGFloat = 0,
                 paddingRight: CGFloat = 0,
                 paddingBottom: CGFloat = 0,
                 width: CGFloat = 0,
                 height: CGFloat = 0) {

        translatesAutoresizingMaskIntoConstraints = false
        
        var topInset = CGFloat(0)
        var bottomInset = CGFloat(0)
        
        let insets = self.safeAreaInsets
        topInset = insets.top
        bottomInset = insets.bottom

        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop + topInset).isActive = true
        }
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom - bottomInset).isActive = true
        }
        if let centerX = centerX {
            centerXAnchor.constraint(equalTo: centerX).isActive = true
        }
        if let centerY = centerY {
            centerYAnchor.constraint(equalTo: centerY).isActive = true
        }
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
    func anchor (top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?, paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat, paddingRight: CGFloat, width: CGFloat, height: CGFloat) {
        var topInset = CGFloat(0)
        var bottomInset = CGFloat(0)
        
        let insets = self.safeAreaInsets
        topInset = insets.top
        bottomInset = insets.bottom
        
        //            print(“Top: \(topInset)”)
        //            print(“bottom: \(bottomInset)”)
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop + topInset).isActive = true
        }
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom - bottomInset).isActive = true
        }
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
    }
}
