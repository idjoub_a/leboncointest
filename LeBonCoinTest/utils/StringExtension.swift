//
//  StringExtension.swift
//  LeBonCoinTest
//
//  Created by Alvaro IDJOUBAR on 25/07/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation

extension String {

    func toDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from:self)
        return date
    }
}
