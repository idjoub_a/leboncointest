//
//  DateExtension.swift
//  LeBonCoinTest
//
//  Created by Alvaro IDJOUBAR on 27/07/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation

extension Date {
    func getCalendarDate() -> String {
        let calendar = Calendar.current
        let day = calendar.component(.day, from: self)
        let month = calendar.component(.month, from: self)
        let year = calendar.component(.year, from: self)
        return "\(day)/\(month)/\(year)"
    }
}
