//
//  MyAttributes.swift
//  LeBonCoinTest
//
//  Created by Alvaro IDJOUBAR on 27/07/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

class MyAttriubtes {
    static func titleAttributes(with title: String, and date: String) -> NSMutableAttributedString{
        let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15), NSAttributedString.Key.foregroundColor : UIColor.black]
        let attrs2 = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15), NSAttributedString.Key.foregroundColor : UIColor.black]
        
        let attributedString1 = NSMutableAttributedString(string: title, attributes:attrs1)
        let attributedString2 = NSMutableAttributedString(string: "\n\(date)", attributes:attrs2)
        
        attributedString1.append(attributedString2)
        return attributedString1
    }
}
