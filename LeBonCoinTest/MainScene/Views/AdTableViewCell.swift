//
//  AdTableViewCell.swift
//  LeBonCoinTest
//
//  Created by Alvaro IDJOUBAR on 24/07/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

struct CellAdViewModel {
    let title: String
    let category: String
    let price: Int
    let isUrgent: Bool
    let date:  Date?
}

class AdTableViewCell: UITableViewCell {
    
    static let cellIdentifier: String = "AdTableViewCell"
    private let sidesPadding: CGFloat = 10
    private let categoryName: String = ""
    
    private let adTitleLabel = UILabel()
    private let adImageView = ProductImageView(image: UIImage(named: "imageTest"))
    private let infoView = InfoView()

    override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .white
        self.layer.shadowOpacity = 0.28
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.masksToBounds = false
        self.selectionStyle = .none
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 20, left: sidesPadding, bottom: 20, right: sidesPadding))
        
        let ratio = (adImageView.image!.size.width - (sidesPadding * 2) ) / adImageView.image!.size.height
        let imageViewHeight = self.frame.width / ratio
        
        addSubview(adTitleLabel)
        addSubview(adImageView)
        addSubview(infoView)
        
        adImageView.anchors(left: leftAnchor, top: topAnchor, right: rightAnchor, centerX: centerXAnchor, paddingLeft: sidesPadding, paddingRight: sidesPadding, height: imageViewHeight)
        
        adTitleLabel.anchors(left: leftAnchor, top: adImageView.bottomAnchor, right: rightAnchor, paddingLeft: sidesPadding * 2, paddingTop: sidesPadding, paddingRight: sidesPadding, height: 40)
                
        infoView.anchors(left: leftAnchor, top: adTitleLabel.bottomAnchor, right: rightAnchor, paddingLeft: sidesPadding * 2, paddingRight: sidesPadding, height: 60)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCell(with adViewModel: CellAdViewModel) {
        let isUrgent = "\(adViewModel.isUrgent ? "🚨": "")"

        adTitleLabel.textAlignment = .left
        adTitleLabel.numberOfLines = 3
        adTitleLabel.attributedText = MyAttriubtes.titleAttributes(with: "\(adViewModel.title)\(isUrgent)", and: adViewModel.date?.getCalendarDate() ?? "No date")
        
        infoView.titleLabel.text = "Categorie"
        infoView.contentLabel.text = "\(adViewModel.category)"
        infoView.priceLabel.text = "\(adViewModel.price == -1 ? "No Price" : adViewModel.price.description) €"
    }
}
