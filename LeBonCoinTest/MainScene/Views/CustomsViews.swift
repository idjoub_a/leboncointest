//
//  CustomsViews.swift
//  LeBonCoinTest
//
//  Created by Alvaro IDJOUBAR on 25/07/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

class TitleLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    private func commonInit() {
        self.textColor = .black
        self.font = UIFont.boldSystemFont(ofSize: 15)
        self.textAlignment = .left
        self.numberOfLines = 3
    }
}

class ProductImageView: UIImageView {
    
    override init(image: UIImage?) {
        super.init(image: image)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentMode = .scaleAspectFit
        self.clipsToBounds = true
    }
    
    private func commonInit() {
    }
}

class ContentLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    private func commonInit() {
        self.textColor = .black
        self.font = UIFont.boldSystemFont(ofSize: 11)
        self.textAlignment = .left
        self.numberOfLines = 3
    }
}

class InfoView: UIView {
    
    let titleLabel = UILabel()
    let contentLabel = UILabel()
    let priceLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    private func setTitleLabel() {
        self.titleLabel.textColor = .orange
        self.titleLabel.font = UIFont.systemFont(ofSize: 14)
        self.titleLabel.textAlignment = .left
        self.titleLabel.numberOfLines = 1
    }

    private func setContentLabel() {
        self.contentLabel.textColor = .black
        self.contentLabel.font = UIFont.systemFont(ofSize: 16)
        self.contentLabel.textAlignment = .left
        self.contentLabel.numberOfLines = 1
    }
    
    private func setPriceLabel() {
        self.priceLabel.textColor = .orange
        self.priceLabel.font = UIFont.boldSystemFont(ofSize: 20)
        self.priceLabel.textAlignment = .right
        self.priceLabel.numberOfLines = 1
    }

    private func commonInit() {
        setTitleLabel()
        setContentLabel()
        setPriceLabel()
        
        addSubview(titleLabel)
        addSubview(contentLabel)
        addSubview(priceLabel)

        titleLabel.anchors(left: leftAnchor, top: topAnchor, right: centerXAnchor, bottom: centerYAnchor)
        contentLabel.anchors(left: leftAnchor, top: titleLabel.bottomAnchor, right: centerXAnchor, bottom: bottomAnchor)
        priceLabel.anchors(left: centerXAnchor, top: centerYAnchor, right: rightAnchor, bottom: bottomAnchor, paddingRight: 5)
    }
}
