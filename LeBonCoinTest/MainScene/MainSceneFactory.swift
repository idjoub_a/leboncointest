//
//  MainSceneFactory.swift
//  LeBonCoinTest
//
//  Created by Alvaro IDJOUBAR on 23/07/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

final class MainSceneFactory {
    static func createModule() -> UIViewController {
        
        let mainVC = MainViewController()
        
        let router = MainRouter()
        let presenter = MainPresenter()
        let interactor = MainInteractor()
        let adsNetworkManager = AdsNetworkManager()
        let worker = MainWorker(networkManager: adsNetworkManager)
        
        interactor.worker = worker
        interactor.presenter = presenter
        mainVC.interactor = interactor
        mainVC.router = router
        presenter.viewController = mainVC
        
        return mainVC
    }
}
