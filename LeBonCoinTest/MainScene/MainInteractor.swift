//
//  MainInteractor.swift
//  LeBonCoinTest
//
//  Created by Alvaro IDJOUBAR on 22/07/2020.
//  Copyright (c) 2020 Alvaro IDJOUBAR. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol MainInteractorInput {
    // Input - Worker Side
    var worker: MainWorkerProtocol?  { get set }
    func fetchAdsInteractor()
    func fetchCategoriesInteractor()
    
    // Input - VC Side
}

protocol MainInteractorOutput {
    // Output - to Presenter
    var presenter: MainPresenterInput? { get set }
}

final class MainInteractor: MainInteractorOutput {
    var presenter: MainPresenterInput?
    var worker: MainWorkerProtocol?
}

extension MainInteractor: MainInteractorInput {
    func fetchAdsInteractor() {
        worker?.fetchAdsWorker(completion: { [unowned self] (result) in
            switch result {
            case .failure(let error):
                self.presenter?.notifyError(error)
            case .success(let ads):
                self.presenter?.notifyAdsSuccess(with: ads)
            }
        })
    }
    
    func fetchCategoriesInteractor() {
        worker?.fetchCategoriesWorker(completion: { [unowned self] (result) in
            switch result {
                case .failure(let error):
                    self.presenter?.notifyError(error)
                case .success(let categories):
                    self.presenter?.notifyCategoriesSuccess(with: categories)

            }
        })
    }
}
