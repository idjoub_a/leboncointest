//
//  MainViewController.swift
//  LeBonCoinTest
//
//  Created by Alvaro IDJOUBAR on 22/07/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

protocol MainViewControllerProtocol: class {
    var interactor: MainInteractorInput? { get set }
    var adsViewModel: AdsViewModel? { get set }
    var categoriesViewModel: [CategoryViewModel]? { get set }
    var router: MainRouter? { get set }
//    func stopActivityIndicator()
}

protocol FilteredDataDelegate {
    func userDidChoose(category: CategoryViewModel)
}

final class MainViewController: UIViewController, MainViewControllerProtocol {

    var router: MainRouter?
    var interactor: MainInteractorInput?
    
    var adsViewModel: AdsViewModel? {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    var categoriesViewModel: [CategoryViewModel]?
    var closure: (UIAlertAction) -> Void {
        return { _ in
            self.fetchData()
        }
    }

    private var filteredAds: AdsViewModel? {
        return selectedCategoryID != 0 ? self.adsViewModel?.filter({ $0.categoryID == selectedCategoryID }) : adsViewModel
    }
    
    private var selectedCategoryID: Int = 0 {
        didSet { self.tableView.reloadData() }
    }
    private let tableView = UITableView()
    private let cellHeight: CGFloat = 450.0
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        setup()
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        tableView.anchors(left: view.leftAnchor, top: view.topAnchor, right: view.rightAnchor, bottom: view.bottomAnchor)

        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.register(AdTableViewCell.self, forCellReuseIdentifier: AdTableViewCell.cellIdentifier)
        self.tableView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 0)
    }

    @objc func openFilterModal() {
        guard let categories = self.categoriesViewModel else { return }
        let vc = FilterViewController()
        vc.delegate = self
        vc.categories = categories
        vc.selectedCategoryID = self.selectedCategoryID
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }

    @objc private func refreshDatas() {
        self.selectedCategoryID = 0
        guard let refreshButton = navigationItem.rightBarButtonItems?.last else { return }
        refreshButton.isEnabled = false
    }
    
    private func fetchData() {
        interactor?.fetchCategoriesInteractor()
        interactor?.fetchAdsInteractor()
    }
    
    private func setup() {
        self.title = "Annonnces"
        fetchData()
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshDatas))
        refresh.isEnabled = false
        let filterButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(openFilterModal))
        navigationItem.rightBarButtonItems = [filterButton, refresh]
        navigationItem.rightBarButtonItems?.forEach({$0.tintColor = .white})
    }
}

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredAds?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: AdTableViewCell = tableView.dequeueReusableCell(withIdentifier: AdTableViewCell.cellIdentifier) as? AdTableViewCell
            else { return UITableViewCell() }
        let categoryName = categoriesViewModel?.first(where: {$0.id == filteredAds?[indexPath.row].categoryID})?.name ?? "No Category Name"
        cell.setupCell(with: CellAdViewModel(title: filteredAds?[indexPath.row].title ?? "No Title",
                                             category: categoryName,
                                             price: filteredAds?[indexPath.row].price ?? -1,
                                             isUrgent: filteredAds?[indexPath.row].isUrgent ?? false,
                                             date : filteredAds?[indexPath.row].creationDate ?? nil))
        return cell
    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    // MARK: Routing
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt \(indexPath.row)")
        //router?.navigateToSomewhere(source: self, destination: DetailSceneFactory.createModule())
        let vc = DetailsViewController()
        vc.adViewModel = filteredAds?[indexPath.row]
        vc.categoriesViewModel = categoriesViewModel
        router?.navigateToDetailsPage(source: self, destination: vc)
    }
}

extension MainViewController: FilteredDataDelegate {
    func userDidChoose(category: CategoryViewModel) {
        // If category already selected -> deselect it
        self.selectedCategoryID = category.id == self.selectedCategoryID ? 0 : category.id ?? 0
        guard let refreshButton = navigationItem.rightBarButtonItems?.last else { return }
        refreshButton.isEnabled = self.selectedCategoryID != 0
    }
}
