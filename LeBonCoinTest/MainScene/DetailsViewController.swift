//
//  DetailsViewController.swift
//  LeBonCoinTest
//
//  Created by Alvaro IDJOUBAR on 27/07/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    var adViewModel: AdViewModel?
    var categoriesViewModel: [CategoryViewModel]?
    
    private let adImageView = ProductImageView(image: UIImage(named: "imageTest"))
    private let adTitleLabel = TitleLabel()
    private let infoView = InfoView()
    private let adPriceLabel = ContentLabel()
    private let adDescriptionTextView = UITextView()

    private let sidesPadding: CGFloat = 20.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setupViews()
        setupContentOfViews()
    }
    
    private func setupContentOfViews() {
        guard let isUrgent = adViewModel?.isUrgent,
              let adTitle = adViewModel?.title else { return }
        
        self.title = "Details de l'annonce"
        let categoryName = categoriesViewModel?.first(where: { $0.id == adViewModel?.categoryID })?.name ?? "No category name"
        
        self.adTitleLabel.text = "\(adTitle) \(isUrgent ? "🚨" : "")"
        self.adTitleLabel.attributedText = MyAttriubtes.titleAttributes(with: "\(adTitle) \(isUrgent ? "🚨" : "")",
                                            and: adViewModel?.creationDate?.getCalendarDate() ?? "No date")
        self.adImageView.sizeToFit()
        self.infoView.titleLabel.text = "Categorie"
        self.infoView.contentLabel.text = categoryName
        self.infoView.priceLabel.text = adViewModel?.price != nil ? "\(adViewModel!.price!)  €" : "No price"
        
        self.adDescriptionTextView.text = adViewModel?.adDescription ?? "No description"
        self.adDescriptionTextView.font = UIFont.systemFont(ofSize: 16)
        self.adDescriptionTextView.sizeToFit()
        self.adDescriptionTextView.isEditable = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.adDescriptionTextView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    private func setupViews() {
        let topdPadding:CGFloat = 180.0

        self.view.addSubview(adImageView)
        self.view.addSubview(adTitleLabel)
        self.view.addSubview(infoView)
        self.view.addSubview(adDescriptionTextView)

        let ratio = adImageView.image!.size.width / adImageView.image!.size.height
        let imageViewHeight = self.view.frame.width / ratio
        
        adImageView.anchors(left: self.view.leftAnchor, top: self.view.topAnchor, right: self.view.rightAnchor, centerX: self.view.centerXAnchor, paddingTop: topdPadding, height: imageViewHeight)

        adTitleLabel.anchors(left: self.view.leftAnchor, top: adImageView.bottomAnchor, right: self.view.rightAnchor, paddingLeft: sidesPadding, paddingTop: sidesPadding, paddingRight: sidesPadding, height: 60)
        
        infoView.anchors(left: self.view.leftAnchor, top: adTitleLabel.bottomAnchor, right: self.view.rightAnchor, paddingLeft: sidesPadding, paddingRight: sidesPadding, height: 60)
        
        adDescriptionTextView.anchors(left: self.view.leftAnchor, top: infoView.bottomAnchor, right: self.view.rightAnchor, bottom: self.view.bottomAnchor, paddingLeft: sidesPadding, paddingTop: 5, paddingRight: sidesPadding)
    }
}
